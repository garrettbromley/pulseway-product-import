﻿using Newtonsoft.Json.Linq;
using RestSharp;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PulsewayProductImport
{
    class Importer
    {
        // Valued Properties
        private string company { get;  set; }
        private string username { get; set; }
        private string password { get; set; }
        private string authorizationKey { get; set; }
        public bool debug { get; set; }

        // Inventory List
        List<Product> inventory = new List<Product>();

        // Excel Values
        private Application excelApp;
        private Workbook excelBook;
        private _Worksheet excelSheet;
        private Range excelRange;

        // Evaluated Properties
        private bool HasCredentials { get { return !(String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(company)); } }
        private bool Authenticated { get { return !String.IsNullOrEmpty(authorizationKey); } }

        // Constructor
        public Importer()
        {
            // Disabled by default unless specified otherwise
            debug = false;
        }

        public void WelcomeMessage()
        {
            Console.Clear();
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine("| Pulseway PSA Inventory Import Tool |");
            Console.WriteLine("|------------------------------------|");
            Console.WriteLine("");
            Console.WriteLine("Developed By:");
            Console.WriteLine("");
            Console.WriteLine("Garrett Bromley");
            Console.WriteLine("techs@maxitpro.com");
            Console.WriteLine("");
            Console.WriteLine("Maximovich IT Consulting");
            Console.WriteLine("www.maxitpro.com");
            Console.WriteLine("+ 1 330 - 315 - 4994");
            Console.WriteLine("");

            AskToContinue();
        }

        // Get credentials from the user
        public void GetCredentials()
        {
            // Clear out the console
            Console.Clear();

            // Title Message
            Console.WriteLine("Please enter your PSA Credentials below.");

            // Ask for username
            while (String.IsNullOrEmpty(username))
            {
                Console.Write("Username: ");
                username = Console.ReadLine();
            }

            if (debug) { Console.WriteLine(String.Format("Inputted Username: {0}", username)); AskToContinue(); }

            // Ask for password
            while (String.IsNullOrEmpty(password))
            {
                Console.Write("Password: ");
                password = Console.ReadLine();
            }

            if (debug) { Console.WriteLine(String.Format("Inputted Password: {0}", password)); AskToContinue(); }

            // Ask for company name
            while (String.IsNullOrEmpty(company))
            {
                Console.Write("Company Name: ");
                company = Console.ReadLine();
            }

            if (debug) { Console.WriteLine(String.Format("Inputted Company: {0}", company)); AskToContinue(); }
        }

        // Authenticate User
        public bool Authenticate()
        {
            // Only run if we aren't authenticated yet
            if (!Authenticated)
            {
                if (debug) { Console.WriteLine("We are not yet authenticated..."); AskToContinue(); }

                // Only run if we have credentials
                if (HasCredentials)
                {
                    if (debug) { Console.WriteLine("We have credentials..."); AskToContinue(); }

                    // Create a new client and request
                    var client = new RestClient("https://psa.pulseway.com/api/token");
                    var request = new RestRequest(Method.POST);

                    // Add headers to the request
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");
                    request.AddHeader("accept", "application/json");

                    // Add credentials to the parameters of the request
                    request.AddParameter("application/x-www-form-urlencoded", String.Format("grant_type=password&username={0}&password={1}&tenant={2}", username, password, company), ParameterType.RequestBody);

                    // Execute the request
                    IRestResponse response = client.Execute(request);

                    // Print the response
                    if (debug) { PrintJSON(response.Content); AskToContinue(); }

                    // If there is an error in the response
                    if (JObject.Parse(response.Content)["error"] != null)
                    {
                        // Tell them why we failed authentication
                        Console.WriteLine("Authentication Failed for Reason: {0}", JObject.Parse(response.Content).SelectToken("error").ToString());
                        AskToContinue();

                        // Clear credentials
                        ClearCredentials();

                        // Get new credentials
                        GetCredentials();

                        // Return status so we can try authenticating again
                        return Authenticated;
                    }

                    // Extract the key and store it
                    var key = JObject.Parse(response.Content).SelectToken("access_token");
                    authorizationKey = key.ToString();

                    if (debug) { Console.WriteLine(String.Format("Authorization Key: {0}", authorizationKey)); AskToContinue(); }
                } else
                {
                    if (debug) { Console.WriteLine("We do not have credentials, asking for them..."); AskToContinue(); }
                    GetCredentials();
                    return Authenticate();
                }
            }

            // Clear out the console
            Console.Clear();

            return Authenticated;
        }

        // Prepare Excel
        public void LoadExcel()
        {
            // Let them know something is happening
            Console.WriteLine("Loading Excel...");

            excelApp = new Application();

            if (excelApp == null)
            {
                Console.WriteLine("Excel Is Not Installed!");
                AskToContinue();
                System.Environment.Exit(-1);
            }
        }

        // Load Excel Sheet
        public void LoadExcelSheet()
        {
            // Initialize directory variable
            string directory = "";

            // Ask for input while invalid
            while (String.IsNullOrEmpty(directory))
            {
                // Clear out the console before asking them for stuff
                Console.Clear();

                // Ask for the directory
                Console.Write("Full Directory of Inventory Excel Sheet: ");
                directory = Console.ReadLine();

                // If it doesn't exist, tell them and reset the variable so it will continue to loop
                if (!File.Exists(directory))
                {
                    Console.WriteLine("That file does not exist. Please try again...");
                    directory = "";
                }
            }

            excelBook = excelApp.Workbooks.Open(directory);
            excelSheet = excelBook.Sheets[1];
            excelRange = excelSheet.UsedRange;
        }

        // Import Products into list
        public void ImportProducts()
        {
            // Calculate the rows and columns
            int rowCount = excelRange.Rows.Count + 1;
            int colCount = excelRange.Columns.Count;

            Console.WriteLine(String.Format("Parsing Excel Document with {0} entries.", rowCount - 2));

            // Ask if this is the right number of products before proceeding
            Console.Write("Is this number correct? (y/n): ");
            string correctRows = Console.ReadLine();
            if (correctRows.ToLower() != "y") { return; }

            // Get the time of starting the import
            DateTime startTime = DateTime.Now;

            // Read through the step by step columns and rows
            for (int i = 2; i < rowCount; i++)
            {
                // Clear out the console and display the time to completion
                Console.Clear();
                TimeSpan timeRemaining = TimeSpan.FromTicks(DateTime.Now.Subtract(startTime).Ticks * ((rowCount - 2) - i) / i);
                if ((i - 1) > 0)
                {
                    Console.WriteLine("Imported: " + (i - 1) + "/" + (rowCount - 2) + " (ETA: " + timeRemaining.TotalMinutes + " minutes)");
                }
                else
                {
                    Console.WriteLine("Imported: " + (i - 1) + "/" + (rowCount - 2) + " (ETA: Forever...)");
                }

                // Defaults before importing
                string productName = "ProductName";
                string productNumber = "123456789";
                string productType = "Normal";
                string productCategory = "Hardware";
                string productSubCategory = "Misc Parts";
                string productBrand = "Dell";
                string productManufacturer = "Dell";
                string productDescription = "Description";
                string productUpc = "123456789";

                // Gather the cells from the sheet
                if (excelRange.Cells[i, 1].Value2 != null) { productName = excelRange.Cells[i, 1].Value2.ToString(); }
                if (excelRange.Cells[i, 2].Value2 != null) { productNumber = excelRange.Cells[i, 2].Value2.ToString(); }
                if (excelRange.Cells[i, 3].Value2 != null) { productType = excelRange.Cells[i, 3].Value2.ToString(); }
                if (excelRange.Cells[i, 4].Value2 != null) { productCategory = excelRange.Cells[i, 4].Value2.ToString(); }
                if (excelRange.Cells[i, 5].Value2 != null) { productSubCategory = excelRange.Cells[i, 5].Value2.ToString(); }
                if (excelRange.Cells[i, 6].Value2 != null) { productBrand = excelRange.Cells[i, 6].Value2.ToString(); }
                if (excelRange.Cells[i, 7].Value2 != null) { productManufacturer = excelRange.Cells[i, 7].Value2.ToString(); }
                if (excelRange.Cells[i, 8].Value2 != null) { productDescription = excelRange.Cells[i, 8].Value2.ToString(); }
                if (excelRange.Cells[i, 9].Value2 != null) { productUpc = excelRange.Cells[i, 9].Value2.ToString(); } else { productUpc = productNumber; }

                inventory.Add(new Product(productName, productNumber, productType, productCategory, productSubCategory, productBrand, productManufacturer, productDescription, productUpc));
            }
        }

        // Send the product list to PSA now
        public void SendProducts()
        {
            // Create the client and request
            var client = new RestClient("https://psa.pulseway.com/api/import/products");
            var request = new RestRequest(Method.POST);

            // Add the approptiate headers
            request.AddHeader("authorization", "Bearer " + authorizationKey);
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            // Set the format and add the inventory body
            request.RequestFormat = DataFormat.Json;
            request.AddBody(inventory);

            // Execute the request
            IRestResponse response = client.Execute(request);

            // Clear out the console to display the response
            Console.Clear();
            Console.WriteLine("Import Results:\n");

            // Print the response
            PrintJSON(response.Content);

            // Add some space at the end
            Console.WriteLine("");
        }

        private void ClearCredentials()
        {
            username = null;
            password = null;
            company = null;
        }

        // Used to print out the JSON easily
        private static void PrintJSON(string json)
        {
            // Parse the JSON string
            JObject parsed = JObject.Parse(json);

            // Loop through all the objects
            foreach (var pair in parsed)
            {
                // Write the key and the value to the console
                Console.WriteLine("{0}: {1}", pair.Key, pair.Value);
            }
        }

        // Used for prompted pauses
        public void AskToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
