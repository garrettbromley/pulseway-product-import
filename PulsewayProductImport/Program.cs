﻿using Newtonsoft.Json.Linq;
using RestSharp;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PulsewayProductImport
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create an instance of our importer
            Importer importer = new Importer();

            // Debug Mode
            // importer.debug = true;

            importer.WelcomeMessage();      // Display some welcome information
            importer.GetCredentials();      // Get the credentials from the users

            bool authenticated = false;
            while (!authenticated)
            {
                authenticated = importer.Authenticate();    // Authenticate using the retreived credentials
            }
            
            importer.LoadExcel();           // Load up Excel
            importer.LoadExcelSheet();      // Load the Excel sheet that we wish to import
            importer.ImportProducts();      // Go through the document and import all the products
            importer.SendProducts();        // Send them through to Pulseway PSA

            importer.AskToContinue();       // Completed and ready to close
        }
    }
}
