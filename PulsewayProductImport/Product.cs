﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulsewayProductImport
{
    class Product
    {
        public string ProductName;
        public string ProductNumber;
        public string ProductType;
        public string ProductCategory;
        public string ProductSubCategory;
        public string ProductBrand;
        public string ProductManufacturer;
        public string ProductDescription;
        public string ProductUpc;
        public string ProductSupplier = "";
        public bool IsTaxable = true;

        public Product(string name, string number, string type, string category, string subCategory, string brand, string mfg, string description, string upc)
        {
            this.ProductName = name;
            this.ProductNumber = number;
            this.ProductType = type;
            this.ProductCategory = category;
            this.ProductSubCategory = subCategory;
            this.ProductBrand = brand;
            this.ProductManufacturer = mfg;
            this.ProductDescription = description;
            this.ProductUpc = upc;
        }
    }
}
